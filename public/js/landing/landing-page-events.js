$(document).ready(() => {
  const SCROLL_UP = 1;
  const SCROLL_DOWN = 2;
  let cancelScroll = false;

  $("#scroll-indicator-main-container").on("mouseenter", function() {
    $(".scroll-indicator-label").css("opacity", "0.6");
  }).on("mouseleave", function() {
    $(".scroll-indicator-label").css("opacity", "0");
  })

  $("#gradient").swipeup(function() {
    scrollDownWrapper()
  })

  $("#gradient").swipedown(function() {
    scrollUpWrapper()
  })

  $("#continue-button").click(function() {
    $("#gradient").css("opacity", "0");
    $("#curtain").css("opacity", "0");
    $(".screen-container").css("opacity", "0");
    $(".background-visible").css("opacity", "0");
  })

  // Bind for chrome and firefox events
  $(document).bind('mousewheel DOMMouseScroll', function(e) {
    // Chrome || Firefox
    if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
      scrollUpWrapper()
    } else {
      scrollDownWrapper()
    }
  });

  $(".scroll-indicator").click(function() {
    if (cancelScroll === true) return;

    cancelScroll = true;
    let targetStepId = $(this).data("step");
    let currentStepId = getCurrentSelectedScrollIndicator();

    if (currentStepId === targetStepId) {
      return;
    }

    changeScrollIndicator(currentStepId, targetStepId, () => {
      cancelScroll = false;
    });

    let direction = getDirection(currentStepId, targetStepId);

    scrollBackground(currentStepId, targetStepId, direction);
  });

  function scrollUpWrapper() {
    // If scene is currently updating, cancel the call
    if (cancelScroll === true) return;

    let currentStepId = getCurrentSelectedScrollIndicator();
    let i = 0;
    let scrollIndicatorBar = $(".scroll-indicator-bar");

    cancelScroll = true;

    // Scroll up
    changeScrollIndicator(currentStepId, currentStepId - 1, () => {
      cancelScroll = false;
    });

    if (currentStepId > 0) {
      scrollBackground(currentStepId, currentStepId - 1, SCROLL_UP);
    }
  }

  function scrollDownWrapper() {
    // If scene is currently updating, cancel the cal
    if (cancelScroll === true) return;

    let currentStepId = getCurrentSelectedScrollIndicator();
    let i = 0;
    let scrollIndicatorBar = $(".scroll-indicator-bar");

    cancelScroll = true;

    // Scroll down
    changeScrollIndicator(currentStepId, currentStepId + 1, () => {
      cancelScroll = false;
    });

    if (currentStepId < $(".scroll-indicator-bar").length - 1) {
      scrollBackground(currentStepId, currentStepId + 1, SCROLL_DOWN);
    }
  }
});