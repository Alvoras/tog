const SCROLL_UP = 1;
const SCROLL_DOWN = 2;

function getCurrentSelectedScrollIndicator() {
  let scrollIndicatorBars = $(".scroll-indicator-bar");

  // Find the current selected scroll indicator
  for (i = 0; i < scrollIndicatorBars.length - 1; i++) {
    if ($(scrollIndicatorBars[i]).hasClass("selected-scroll-bar")) {
      break;
    }
  }

  return i;
}

function getDirection(currentStepId, targetStepId) {
  return (currentStepId > targetStepId) ? SCROLL_UP : SCROLL_DOWN;
}

function changeScrollIndicator(currentStepId, targetStepId, cb) {
  // Callback is needed since we need to pass the reference of the cancelScroll var from the events page

  let scrollIndicatorBars = $(".scroll-indicator-bar");
  let scrollIndicatorLabels = $(".scroll-indicator-label");

  let direction = getDirection(currentStepId, targetStepId)

  switch (direction) {
    case SCROLL_UP:
      if (currentStepId <= 0) {
        // Timeout to avoid scroll trigger overload
        setTimeout(() => {
          cb();
        }, 100);
        return
      };
      break;
    case SCROLL_DOWN:
      if (currentStepId >= scrollIndicatorBars.length - 1) {
        // Timeout to avoid scroll trigger overload
        setTimeout(() => {
          cb();
        }, 100);
        return
      }
      break;
  }

  scrollIndicatorLabels.css("opacity", "0.6");

  // Apply animation
  $($(".arrow-down")[currentStepId]).removeClass("animate-arrow");
  $($(".arrow-down")[targetStepId]).addClass("animate-arrow");
  $(scrollIndicatorBars[currentStepId]).removeClass("selected-scroll-bar");
  $(scrollIndicatorBars[targetStepId]).addClass("selected-scroll-bar");

  $(scrollIndicatorLabels[currentStepId]).removeClass("selected-indicator-label");
  $(scrollIndicatorLabels[targetStepId]).addClass("selected-indicator-label");

  // Re-enable scroll after animation (1sec)
  // !! Must be synced with transition time of .selected-scroll-bar and .scroll-indicator-bar in elevator.css

  setTimeout(() => {
    scrollIndicatorLabels.css("opacity", "0");
    cb();
  }, 1200);
}

function scrollBackground(currentStepId, nextStepId, direction) {
  let enterButtons = $(".enter-container");

  // Remove all scene-visible blurred background
  $('.background-blur-fadable').removeClass("background-blur-visible");
  // Add blurred version of next background
  $(`.background-blur-fadable.step-${nextStepId}`).addClass("background-blur-visible");
  // Position the transparent next background
  $(`.background-fadable.step-${nextStepId}`).addClass("background-visible");

  // Fade out current bg
  $(`.background-fadable.step-${currentStepId}`).removeClass("background-fade-in");
  $(enterButtons[currentStepId]).removeClass("enter-container-visible");

  // Remove current (now transparent) background from visible area
  setTimeout(() => {
    $(`.background-fadable.step-${currentStepId}`).removeClass("background-visible");
  }, 800);

  // Fade in next background
  setTimeout(() => {
    $(enterButtons[nextStepId]).addClass("enter-container-visible");
    $(`.background-fadable.step-${nextStepId}`).addClass("background-fade-in");
  }, 800);

  // TEXT ANIMATION
  let nextTextContainer = $(`.text-container.step-${nextStepId}`);
  let currentTextContainer = $(`.text-container.step-${currentStepId}`);

  // Default : if direction === SCROLL_DOWN
  let currentSlideClass = "slide-text-container-down";
  let nextSlideClass = "slide-text-container-up";

  if (direction === SCROLL_UP) {
    currentSlideClass = "slide-text-container-up";
    nextSlideClass = "slide-text-container-down";
  }

  setTimeout(() => {
    // Position the transparent next text container
    nextTextContainer.addClass(`${nextSlideClass} text-container-visible`);
    // Slide and fade out the next text
    // Timeout
    setTimeout(() => {
      nextTextContainer.removeClass(`${nextSlideClass} text-container-fade-out`);
    }, 1000);

    // Slide and fade in the next text
    currentTextContainer.addClass(`${currentSlideClass} text-container-fade-out`);
    // Reset the state of the text moving away
    setTimeout(() => {
      currentTextContainer.removeClass(`${currentSlideClass} text-container-visible`)
    }, 1000);
  }, 150);
}