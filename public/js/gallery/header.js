$(document).ready(function() {
  $(window).scroll(function(event) {
    var scrollAmount = $(window).scrollTop();
    var headerHeight = $("header").height();
    var scrollPercent = (scrollAmount / headerHeight) * 100;

    $("header").css("opacity", ((100 - (scrollPercent * 1.5)) / 100).toString())
  });
})