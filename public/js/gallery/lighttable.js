// function shutterSplit() {
//   // $("#top-shutter").css("top", "-50%");
//   // $("#bottom-shutter").css("bottom", "-50%");
//   $("#top-shutter").addClass("top-open");
//   $("#bottom-shutter").addClass("bottom-open");
// }
//
// function shutterShut() {
//   // $("#top-shutter").css("top", "0");
//   // $("#bottom-shutter").css("bottom", "0");
//   $("#top-shutter").removeClass("top-open");
//   $("#bottom-shutter").removeClass("bottom-open");
// }
//
// function toggleShutter() {
//   if ($("#top-shutter").hasClass("top-open")) {
//     shutterShut();
//   } else {
//     shutterSplit();
//   }
// }

function animateRow() {
    let rowWrappers = Array.from(document.getElementsByClassName('row-wrapper'));

    rowWrappers.forEach(function (row, count) {
        let i = 0;
        let rows = row.getElementsByClassName("th-row")

        setTimeout(function() {
            let timer = setInterval(function() {
                if (i === rows.length - 1 ) {
                    clearInterval(timer); // STOP THE LOOP
                }
                i % 2 === 0 ? placeLeft(rows[i], i) : placeRight(rows[i], i);
                i++;
            }, 400*(count+1));
        }, 1500);
    })

    for (let row of rowWrappers){
    }
}

function next() {
    let lightboxImage = $("#lightbox-image")
    let src = lightboxImage.attr("src")
    let id = lightboxImage.data("th-id")
    let idLen = id.length
    let numId = Number(id)
    let newIdStr = String(numId+1)
    let thumbnailsQty = $(`img[data-th-album='${lightboxImage.data("th-album")}']`).length -1

    let padLen = 2 - idLen
    let newSrc = src.replace("thumbnails", "galleries")
    newSrc = src.replace(`${id}.jpg`, newIdStr.padStart(padLen, '0')+".jpg")

    if (id < thumbnailsQty) {
        lightboxImage.css("opacity", "0");
        setTimeout(function() {
            lightboxImage.attr("src", newSrc);
            lightboxImage.data("th-id", newIdStr)
            lightboxImage.on("load", function() {
                setTimeout(function() {
                    lightboxImage.css("opacity", "1");
                }, 200);
            });
        }, 500);
    }
}


function prev() {
    let lightboxImage = $("#lightbox-image")
    let src = lightboxImage.attr("src");
    let id = lightboxImage.data("th-id")
    let idLen = id.length
    let numId = Number(id)
    let newIdStr = String(numId-1)

    let padLen = 2 - idLen

    let newSrc = src.replace("thumbnails", "galleries")
    newSrc = src.replace(`${id}.jpg`, newIdStr.padStart(padLen, '0')+".jpg")

    if (id > 1) {
        lightboxImage.css("opacity", "0");
        setTimeout(function() {
            lightboxImage.attr("src", newSrc);
            lightboxImage.data("th-id", newIdStr)
            lightboxImage.on("load", function() {
                setTimeout(function() {
                    lightboxImage.css("opacity", "1");
                }, 200);
            });
        }, 500);
    }
}

function placeRight(el, i) {
    el.style.right = `${4 - x[i]}vw`;
    setTimeout(function() {
        el.style.transitionDuration = ".2s";
    }, 1000);
}

function placeLeft(el, i) {
    el.style.left = `${-x[i]}vw`;
    setTimeout(function() {
        el.style.transitionDuration = ".2s";
    }, 1000);
}

$(document).ready(function() {
    // NEED TO APPLY OPACITY MANUALLY IF THE CLIENT HAS BEEN
    // BROWSING ANOTHER TAB THUS NOT TRIGGERING THE LOADED EVENT (?)
    // setTimeout(function() {
    //   if ($('#th-container').css("opacity") != 1) {
    //     $('#th-container').css('opacity', "1");
    //   }
    // }, 3000);

    $(window).on("keyup", function(event) { // LEFT 37 RIGHT 39
        if (event.keyCode === 37) {
            prev();
        } else if (event.keyCode === 39) {
            next();
        } else if (event.keyCode === 27) {
            $("#lightbox").removeClass("lightbox-display");
            if ($('#screen').html() !== '') {
                $('#screen').text('');
            }
        }
    });

    $("#lightbox").swipeleft(function() {
        next();
    })

    $("#lightbox").swiperight(function() {
        prev();
    })

    // --- ON PAGE LOAD ANIMATION ---
    // $('.lt-thumbnail').on("load", function() {
    //   $('#th-container').css('opacity', "1");
    // });

    $('.lt-thumbnail').click(function() {
        let curGallery = $("#header-title").data("dirname")
        // let curId = $(this).attr("src");
        let thID = $(this).data("th-id")
        let thAlbumName = $(this).data("th-album")
        let imageStyle = $(this).hasClass("vertical-image") ? "vertical-image": "horizontal-image"
        let src = $(this).attr("src");
        src = src.replace("thumbnails", "galleries")
        // curId = curId.replace("thumbnails/" + curGallery + "/thimg", "").replace(".jpg", "");
        // console.log(curId);
        $('#screen').append(`<img src="${src}" id="lightbox-image" data-th-album="${thAlbumName}" data-th-id="${thID}" alt="image" >`)
        $('#screen').append('<div id="preloader"></div>');
        $('#lightbox').addClass("lightbox-display");
        $('#lightbox-image').on("load", function() {
            $('#preloader').fadeOut("slow", function() {
                $(this).remove();
            });
            setTimeout(function() {
                $('#lightbox-image').animate({
                    "opacity": "1"
                }, 200);
            }, 500);
        });
    });

    $('#lightbox').click(function() {
        $(this).removeClass("lightbox-display");
        if ($('#screen').html() !== '') {
            $('#screen').text('');
        }
    });

    // --- HOVER LEFT ANIMATION ---
    $('.first-left').hover(function() {
        let curRow = $(this).parent();
        curRow.css({
            "left": "0",
            "transition-duration": ".2s"
        });
    }, function() {
        let curRow = $(this).parent();
        let id = curRow.data("row-id");
        curRow.css("left", `${-x[id]}vw`);
    });

    $('.last-left').hover(function() {
        let curRow = $(this).parent();
        let leftOffset = $(this).position().left;
        let windowWidth = $(window).width();
        let vpWidth = windowWidth / 100;
        let visible = windowWidth - leftOffset;
        let divWidth = $(this).innerWidth();
        let rightOverflow = divWidth - visible;
        let rightMargin = 6 / vpWidth; // RIGHT MARGIN
        curRow.css({
            "left": `${-rightOverflow / vpWidth - rightMargin}vw`,
            "transition-duration": ".2s"
        });
    }, function() {
        let curRow = $(this).parent();
        let id = curRow.data("row-id");
        curRow.css("left", `${-x[id]}vw`);
    });

    // --- HOVER RIGHT ANIMATION ---
    $('.last-right').hover(function() {
        let curRow = $(this).parent();
        let leftOffset = $(this).position().left;
        let windowWidth = $(window).width();
        let vpWidth = windowWidth / 100;
        let visible = windowWidth - leftOffset;
        let divWidth = $(this).innerWidth();
        let rightOverflow = divWidth - visible;
        let rightMargin = 6 / vpWidth; // RIGHT MARGIN

        curRow.css({
            "right": `${rightOverflow / vpWidth + rightMargin}vw`,
            "transition-duration": ".2s"
        });
    }, function() {
        let curRow = $(this).parent();
        let id = curRow.data("row-id");
        curRow.css("right", `${4 - x[id]}vw`);
    });

    $('.first-right').hover(function() {
        let curRow = $(this).parent();
        curRow.css({
            "right": "0",
            "transition-duration": ".2s"
        });
    }, function() {
        let curRow = $(this).parent();
        let id = curRow.data("row-id");
        curRow.css("right", `${4 - x[id]}vw`);
    });

    animateRow();
});