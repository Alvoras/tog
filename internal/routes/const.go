package routes

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

var (
	Categories = []string{
		//"Graphisme",
		//"Aviation",
		//"Loi travail",
		//"Nuit debout",
	}
	CategoriesMap = map[string]string{}
	DirnamesMap = map[string]string{}
	Dirnames    = []string{}
	OrderedDirnames    = make(map[int]string)
)

func init() {
	var category string
	var dirname string

	files, err := ioutil.ReadDir("public/img/galleries")
    if err != nil {
        panic(err)
    }

	for _, file := range files{
		if !file.IsDir() {
			continue
		}

		dirChunks := strings.Split(file.Name(), "_")
		order, err := strconv.Atoi(dirChunks[0])
		if err != nil {
			fmt.Println(file.Name(), "is not a valid gallery name")
			os.Exit(1)
		}
		dirname = dirChunks[1]
		category = DirnameToTitleName(dirname)

		Dirnames = append(Dirnames, dirname)
		Categories = append(Categories, category)
		CategoriesMap[category] = dirname
		DirnamesMap[dirname] = category
		OrderedDirnames[order] = dirname
	}
}
