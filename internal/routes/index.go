package routes

import (
	"gitlab.com/Alvoras/tog/internal/config"
	"html/template"
	"log"
	"net/http"
)

func Index(w http.ResponseWriter, r *http.Request) {
	type IndexPage struct {
		Title      string
		Categories []string
		Dirnames    []string
	}

	page := IndexPage{
		Title:      config.Cfg.Title,
		Categories: Categories,
		Dirnames: Dirnames,
	}

	funcMap := template.FuncMap{
		// The name "inc" is what the function will be called in the template text.
		"inc": func(i int) int {
			return i + 1
		},
		"EchoIfFirst": func(echo string, index int) string {
			if index == 0 {
				return echo
			}

			return ""
		},
		"EchoIfNotFirst": func(echo string, index int) string {
			if index != 0 {
				return echo
			}

			return ""
		},
	}

	tmpl := template.New("index").Funcs(funcMap)

	tmpl = template.Must(
		tmpl.ParseFiles(
			"views/index.tmpl",
			"views/inc/global/head.tmpl",
			"views/inc/global/footer.tmpl",
			"views/inc/global/js-lib.tmpl",
		),
	)

	if err := tmpl.ExecuteTemplate(w, "index", page); err != nil {
		log.Fatal("Failed to execute the template :", err)
	}
}
