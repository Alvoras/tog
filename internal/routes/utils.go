package routes

import (
	"image"
	_ "image/jpeg"
	"math/rand"
	"os"
	"strings"
	"time"
)

func GalleryNameToDirName(gallery string) string {
	dirname := strings.Replace(gallery, " ", "-", -1)
	dirname = strings.ToLower(dirname)

	return dirname
}

func DirnameToTitleName(dirname string) string {
	galleryName := strings.Replace(dirname, "-", " ", -1)
	galleryName = strings.Title(galleryName)

	return galleryName
}

func GenRandInt(min int, max int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(max-min+1) + min
}

func isVertical(path string) bool {
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}

	m, _, err := image.Decode(f)
	if err != nil {
		panic(err)
	}
	g := m.Bounds()

	// Get height and width
	height := g.Dy()
	width := g.Dx()

	if height > width {
		return true
	}

	return false
}
