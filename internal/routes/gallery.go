package routes

import (
	"fmt"
	"github.com/briandowns/spinner"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/Alvoras/tog/internal/logger"

	"gitlab.com/Alvoras/tog/internal/config"

	"gitlab.com/Alvoras/tog/internal/gallery"

	"github.com/gorilla/mux"
)

type GalleryPage struct {
	Title           string
	Categories      []string
	CategoriesMap   map[string]string
	DirnamesMap     map[string]string
	CurrentCategory string
	Galleries       gallery.Galleries
	CurrentDirname  string
	Offsets         []int
	ImagesPerRow    int
	Lighttable      template.HTML
}

var (
	galleries       gallery.Galleries
	lighttables     = make(map[string]template.HTML)
	totalOffsetsIdx = 0
)

func init() {
	logger.Info.Println("Building galleries...")
	s := spinner.New(spinner.CharSets[12], 100*time.Millisecond)  // Build our new spinner
	s.Start()                                               // Start the spinner
	for idx, dirname := range OrderedDirnames {
		albums := &gallery.Albums{}
		title := DirnameToTitleName(dirname)

		err = filepath.Walk(fmt.Sprintf("public/img/galleries/%d_%s", idx, dirname), func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			} else if info.IsDir() {
				return nil
			} else if !strings.Contains(path, "fullsize") {
				return nil
			}

			pathChunks := strings.Split(filepath.Dir(path), string(filepath.Separator))
			albumTitle := DirnameToTitleName(pathChunks[len(pathChunks)-2])

			if _, err := albums.GetAlbumByTitle(albumTitle); err != nil {
				albums.NewAlbum(albumTitle)
			}

			albums.AddToAlbum(albumTitle, path)
			return nil
		})

		galleries.AddGallery(gallery.NewGallery(albums, title))

		galleryName := DirnamesMap[dirname]
		var htmlChunks []string

		gal, err := galleries.GetByTitle(galleryName)
		if err != nil {
			logger.Error.Println(err)
			continue
		}

		for _, album := range *gal.Albums {
			htmlChunks = append(htmlChunks, buildLighttable(*album))
		}

		lighttables[dirname] = template.HTML(strings.Join(htmlChunks, "\n"))
	}

	s.Stop()

	return
}

func Gallery(w http.ResponseWriter, r *http.Request) {
	CurrentDirname := mux.Vars(r)["dirname"]
	CurrentCategory := DirnamesMap[CurrentDirname]

	if _, err := galleries.GetByTitle(CurrentCategory); err != nil {
		w.WriteHeader(404)
		w.Write([]byte("404 not found"))
		return
	}

	page := GalleryPage{
		Title:           "Gallerie",
		Categories:      Categories,
		Galleries:       galleries,
		CurrentDirname:  CurrentDirname,
		CategoriesMap:   CategoriesMap,
		DirnamesMap:     DirnamesMap,
		CurrentCategory: CurrentCategory,
		ImagesPerRow:    config.Cfg.ImagesPerRow,
		Lighttable:      lighttables[CurrentDirname],
	}
	//page.RowsQuantity = page.TotalPhotos / page.ImagesPerRow

	funcMap := template.FuncMap{
		"EchoIfStringEq": func(a string, b string, echoTrue string, echoFalse string) string {
			if a == b {
				return echoTrue
			}

			return echoFalse
		},
		//"buildLighttable": func() template.HTML {
		//
		//},
	}

	tmpl := template.New("gallery").Funcs(funcMap)
	// tmpl := template.New("gallery")

	tmpl = template.Must(
		tmpl.ParseFiles(
			"views/gallery.tmpl",
			"views/inc/gallery/lighttable.tmpl",
			"views/inc/global/head.tmpl",
			"views/inc/global/footer.tmpl",
			"views/inc/global/js-lib.tmpl",
		),
	)

	if err := tmpl.ExecuteTemplate(w, "gallery", page); err != nil {
		log.Fatal("Failed to execute the template :", err)
	}
}

func buildLighttable(album gallery.Album) string {
	var thRowSide string
	var imageIndex int
	var html []string
	var rowID = 0
	var rowIndex int
	var lastRow string

	html = append(html, "<div class='row-wrapper'>")
	html = append(html, fmt.Sprintf("<h3 class='album-title'>%s</h3>", album.Title))

	for rowIndex = 0; rowIndex < album.RowsQuantity+1; rowIndex++ {
		thRowSide = "th-row-left"
		if rowIndex%2 != 0 {
			thRowSide = "th-row-right"
		}

		if rowIndex == album.RowsQuantity {
			lastRow = "last-row"
		}

		imgHTMLLine := fmt.Sprintf(`<div class="th-row %s %s" data-row-id="%d" style="transition-duration: 1s">`, thRowSide, lastRow, rowID)
		html = append(html, imgHTMLLine)
		html = append(html, fmt.Sprintf(`<script>x.push(%f)</script>`, config.Cfg.Offsets[rowIndex+totalOffsetsIdx]))
		html = append(html, buildThumbnails(rowIndex, imageIndex, album))
		html = append(html, "</div>")
		rowID++
		imageIndex += config.Cfg.ImagesPerRow
	}
	totalOffsetsIdx += rowIndex
	html = append(html, "</div>")

	return strings.Join(html, "\n")
}

func buildThumbnails(rowIndex int, imageIndex int, album gallery.Album) string {
	var htmlThumbnails []string
	var varClass string
	var imageNumber int64

	placerFirst := "first-right"
	placerLast := "last-right"

	if rowIndex%2 == 0 {
		placerFirst = "first-left"
		placerLast = "last-left"
	}

	for j := 0; j < config.Cfg.ImagesPerRow; j++ {
		imageStyle := "horizontal-image"
		if imageIndex == len(album.Photos) {
			break
		}

		if j == 0 {
			varClass = placerFirst
		} else if j == config.Cfg.ImagesPerRow-1 {
			varClass = placerLast
		} else {
			varClass = ""
		}

		if isVertical(strings.Replace(album.Photos[imageIndex], "fullsize", "thumbnails", 1)) {
			imageStyle = "vertical-image"
		}

		chunks := strings.Split(album.Photos[imageIndex], string(filepath.Separator))
		fmt.Sscanf(chunks[len(chunks)-1], "%d.jpg", &imageNumber)

		htmlThumbnails = append(htmlThumbnails, fmt.Sprintf(`<img src="/%s" class="lt-thumbnail %s %s" data-th-album="%s" data-th-id="%d" alt="thumbnail">`, album.Photos[imageIndex], varClass, imageStyle, album.Title, imageNumber))

		imageIndex++
	}

	return strings.Join(htmlThumbnails, "\n")
}
