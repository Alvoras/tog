package routes

import (
	"net/http"

	"github.com/gorilla/mux"
)

var (
	err    error
	Router *mux.Router
)

func init() {
	Router = mux.NewRouter()

	Router.HandleFunc("/", Index).Methods("GET")
	Router.HandleFunc("/gallery/{dirname}", Gallery).Methods("GET")
	Router.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("public"))))
}
