package gallery

import "fmt"

type Galleries []*Gallery

func (galleries *Galleries) AddGallery(gal *Gallery)  {
	*galleries = append(*galleries, gal)
}

func (galleries *Galleries) GetByTitle(galleryTitle string) (*Gallery, error)  {
	for _, gal := range *galleries{
		if gal.Title == galleryTitle{
			return gal, nil
		}
	}

	return nil, fmt.Errorf("%s: album not found", galleryTitle)
}
