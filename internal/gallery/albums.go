package gallery

import (
	"fmt"
	"gitlab.com/Alvoras/tog/internal/config"
)

type Albums []*Album

func (albums *Albums) NewAlbum(albumName string) {
	new := &Album{
		Title: albumName,
	}

	*albums = append(*albums, new)
}

func (albums *Albums) AddToAlbum(albumName string, path string) error {
	alb, err := albums.GetAlbumByTitle(albumName)
	if err != nil {
		return err
	}

	alb.Photos = append(alb.Photos, path)

	// Get the upper integer
	//alb.RowsQuantity = int(math.Ceil(float64(len(alb.Photos)) / float64(config.Cfg.ImagesPerRow)))
	alb.RowsQuantity = len(alb.Photos) / config.Cfg.ImagesPerRow
	return nil
}

func (albums *Albums) GetAlbumByTitle(albumTitle string) (*Album, error) {
	for _, alb := range *albums {
		if alb.Title == albumTitle {
			return alb, nil
		}
	}

	return nil, fmt.Errorf("%s: album not found", albumTitle)
}
