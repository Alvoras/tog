package gallery

type Album struct {
	Title        string
	RowsQuantity int
	Photos       []string
}

func (alb *Album) AddPhoto(path string)  {
	alb.Photos = append(alb.Photos, path)
}