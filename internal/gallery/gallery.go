package gallery

import "fmt"

type Gallery struct{
	Title string
	Albums *Albums
}

func NewGallery(albums *Albums, title string) *Gallery {
	gallery := new(Gallery)

	gallery.Title = title
	gallery.Albums = albums

	return gallery
}

func (gallery *Gallery) GetAlbumByTitle(albumTitle string) (*Album, error)  {
	for _, alb := range *gallery.Albums{
		if alb.Title == albumTitle{
			return alb, nil
		}
	}

	return nil, fmt.Errorf("%s: album not found", albumTitle)
}
