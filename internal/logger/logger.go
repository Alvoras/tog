package logger

import (
	"io"
	"log"
	"os"

	"gitlab.com/Alvoras/tog/internal/config"
)

var (
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
	Success *log.Logger
	HTTP    *log.Logger
)

func init() {
	var infoWriter io.Writer = os.Stdout
	var warningWriter io.Writer = os.Stdout
	var errorWriter io.Writer = os.Stdout
	var successWriter io.Writer = os.Stdout

	Success = log.New(successWriter,
		"[+] ",
		0,
	)

	Info = log.New(infoWriter,
		"[i] ",
		0,
	)

	Warning = log.New(warningWriter,
		"[!] ",
		0,
	)

	Error = log.New(errorWriter,
		"[x] ",
		0,
	)
}

func InitHTTPLogging(useStdout bool) {
	var HTTPWriter io.Writer

	if useStdout {
	}

	f, err := os.OpenFile(config.Cfg.Server.HTTPLogPath, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if useStdout {
		Info.Println("HTTP requests will be sent to stdout")
		HTTPWriter = os.Stdout
	} else if err != nil {
		Warning.Println(err)
		Warning.Println("Failed to open the HTTP log file. Defaulting to stdout")
		HTTPWriter = os.Stdout
	} else {
		HTTPWriter = f
	}

	HTTP = log.New(HTTPWriter,
		"[>] ",
		0,
	)
}
