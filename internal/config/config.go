package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

var Cfg Config

// Config structure which mirrors the json file
type Config struct {
	Server struct {
		HTTP struct {
			Port int64 `json:"port"`
		} `json:"http"`
		HTTPS struct {
			Port int64 `json:"port"`
		} `json:"https"`
		HTTPLogPath string `json:"logPath"`
	} `json:"server"`
	Timeout struct {
		Write string `json:"write"`
		Read  string `json:"read"`
		Idle  string `json:"idle"`
	} `json:"timeout"`
	Title string `json:"title"`
	ImagesPerRow int `json:"imagesPerRow"`
	Offsets []float32 `json:"offsets"`
}

func loadConfigFileJSON() Config {
	var c Config
	configPath, err := filepath.Abs("config/config.json")
	if err != nil {
		log.Fatal(fmt.Sprintf("Failed to load the config file (%s) : %v", configPath, err))
		log.Fatal(err)
		os.Exit(1)
	}

	cfgData, err := ioutil.ReadFile(configPath)
	if err != nil {
		log.Fatal(fmt.Sprintf("Failed to load the config file (%s) : %v", configPath, err))
		log.Fatal(err)
		os.Exit(1)
	}

	if err := json.Unmarshal(cfgData, &c); err != nil {
		log.Fatal(fmt.Sprintf("Failed to load the config file (%s) : %v", configPath, err))
		log.Fatal(err)
		os.Exit(1)
	}

	return c
}

func init() {
	Cfg = loadConfigFileJSON()
}
