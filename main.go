package main

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"time"

	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/Alvoras/tog/internal/config"
	"gitlab.com/Alvoras/tog/internal/logger"
	r "gitlab.com/Alvoras/tog/internal/routes"
)

type App struct {
	Env     string
	DoHTTPS bool
}

var (
	tlsCfg      *tls.Config
	certPath    string
	keyPath     string
	wrTimeout   time.Duration
	rdTimeout   time.Duration
	idleTimeout time.Duration
)

func init() {
	var err error

	wrTimeout, err = time.ParseDuration(config.Cfg.Timeout.Write)
	if err != nil {
		log.Fatal(err)
		return
	}

	rdTimeout, err = time.ParseDuration(config.Cfg.Timeout.Read)
	if err != nil {
		log.Fatal(err)
		return
	}

	idleTimeout, err = time.ParseDuration(config.Cfg.Timeout.Idle)
	if err != nil {
		log.Fatal(err)
		return
	}
}

func main() {
	logger.Info.Println(fmt.Sprintf("Starting HTTP at %d...", config.Cfg.Server.HTTP.Port))
	httpSrv := &http.Server{
		Addr:         fmt.Sprintf("0.0.0.0:%d", config.Cfg.Server.HTTP.Port),
		WriteTimeout: wrTimeout,
		ReadTimeout:  rdTimeout,
		IdleTimeout:  idleTimeout,
		Handler:      r.Router,
	}


	logger.Error.Println(httpSrv.ListenAndServe())
}
